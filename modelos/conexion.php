<?php

class Conexion{

    static public function conectar(){

        //conexion segura PDo
        $link = new PDO("mysql:host=localhost;dbname=nantiva_pos", //nombre de servidor y bd
                            "root", // usuario administrador de bd
                            ""); // password administrador de bd

        $link->exec("set names utf8"); // ejecucción de caracteres latinos 

        return $link;

    }
}