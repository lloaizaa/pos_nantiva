<?php
// clase principal para ususarios
class ControladorUsuarios{

    /* ===============================
            Ingreso de Usuarios 
        ================================== */

        static public function ctrIngresoUsuario(){

            if(isset($_POST["ingUsuario"])){

                // expresion regular para solo permitir letras y numeros
                if(preg_match('/^[a-zA-Z0-9]+$/',$_POST["ingUsuario"]) &&
                    preg_match('/^[a-zA-Z0-9]+$/',$_POST["ingPassword"])){

                        $passwordEncriptada = crypt($_POST["ingPassword"], '$2a$07$asxx54ahjppf45sd87a5a4dDDGsystemdev$');//validar encriptacion de password

                        $tabla = "usuarios"; // tabla de la BD a consultar
                        $item = "usuario"; //Colunma de la tabla a consultar
                        $valor = $_POST["ingUsuario"]; //atributo name en formulario

                        $respuesta = ModeloUsuarios::MdlMostrarUsuarios($tabla, $item, $valor); // invoca al modelo

                        if($respuesta["usuario"] == $_POST["ingUsuario"] && 
                            $respuesta["password"] == $passwordEncriptada){

                                //validar estado para permitir ingresar
                                if($respuesta["estado"] == 1){

                                    // si coinciden datos y esta en estado 1 en BD redireccionar a inicio
                                $_SESSION["iniciarSesion"] = "ok";
                                $_SESSION["id"] = $respuesta["id"];
                                $_SESSION["nombre"] = $respuesta["nombre"];
                                $_SESSION["usuario"] = $respuesta["usuario"];
                                $_SESSION["foto"] = $respuesta["foto"];
                                $_SESSION["perfil"] = $respuesta["perfil"];
                                $_SESSION["fecha"] = $respuesta["fecha"];

                                //capturar fecha y hora para registrar logueo
                                date_default_timezone_set('America/Bogota');

                                $fecha = date('Y-m-d');
                                $hora = date('H:i:s');

                                $fechalogin = $fecha.' '.$hora;

                                $item1 = "ultimo_login";
                                $valor1 = $fechalogin;

                                $item2 = "id";
                                $valor2 = $respuesta["id"]; 

                                $ultimoLogin = ModeloUsuarios::mdlActualizarUsuario($tabla, $item1, $valor1, $item2, $valor2);

                                if($ultimoLogin == "ok"){
                                    //redirigir a inicio
                                    echo '<script>
                                            window.location = "inicio";
                                        </script>';
                                }                                 
                                
                                }else{ // si coinciden datos y esta en estado 0 en BD negar acceso
                                    echo '<br><div class="callout callout-warning">!El usuario está inactivo, por favor validar con el administrador del sistema¡</div>';
                                }

                        }else{ 
                            // si no coincide datos con los de la BD 
                            echo '<br><div class="callout callout-danger">Error al ingresar, vuelve a intentarlo</div>';
                        }
                }
            }
        }

    /* ===============================
            Crear  Usuario
        ================================== */

        static public function ctrCrearUsuario(){

            // ejecutar solo si viene informacion
            if(isset($_POST["nuevoUsuario"])){

                // expresion regular para solo permitir letras, numeros y vocales tildadas
                if(preg_match('/^[a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ ]+$/',$_POST["nuevoNombre"]) &&
                    preg_match('/^[a-zA-Z0-9]+$/',$_POST["nuevoUsuario"]) &&
                    preg_match('/^[a-zA-Z0-9]+$/',$_POST["nuevoPassword"])){

                        // Validar imagen
                        $ruta = "";
                        if(isset($_FILES["nuevaFoto"]["tmp_name"])){

                            //Redimensionar imagen
                            list($ancho, $alto) = getimagesize($_FILES["nuevaFoto"]["tmp_name"]);
                            $nuevoAncho = 500;
                            $nuevoAlto = 500;

                            //Crear directorio de imagen
                            $directorio = "vistas/img/usuarios/".$_POST["nuevoUsuario"];
                            mkdir($directorio, 0755); //crea el directorio con permisos de lectura y escritura

                            //aplicar metodos imagen jpeg
                            if($_FILES["nuevaFoto"]["type"] == "image/jpeg"){
                                //guardar imagen
                                $aleatorio = mt_rand(100,999);
                                $ruta = "vistas/img/usuarios/".$_POST["nuevoUsuario"]."/".$aleatorio.".jpg";

                                $origen = imagecreatefromjpeg($_FILES["nuevaFoto"]["tmp_name"]);
                                $destino = imagecreatetruecolor($nuevoAncho, $nuevoAlto);

                                imagecopyresized($destino, $origen, 0,0,0,0, $nuevoAncho, $nuevoAlto, $ancho, $alto);
                                imagejpeg($destino, $ruta);
                            }

                            //aplicar metodos imagen png
                            if($_FILES["nuevaFoto"]["type"] == "image/png"){
                                //guardar imagen
                                $aleatorio = mt_rand(100,999);
                                $ruta = "vistas/img/usuarios/".$_POST["nuevoUsuario"]."/".$aleatorio.".png";

                                $origen = imagecreatefrompng($_FILES["nuevaFoto"]["tmp_name"]);
                                $destino = imagecreatetruecolor($nuevoAncho, $nuevoAlto);

                                imagecopyresized($destino, $origen, 0,0,0,0, $nuevoAncho, $nuevoAlto, $ancho, $alto);
                                imagepng($destino, $ruta);
                            }

                        }

                        // Almacenar en BD
                        $tabla = "usuarios"; // tabla de la BD a consultar

                        $passwordEncriptada = crypt($_POST["nuevoPassword"], '$2a$07$asxx54ahjppf45sd87a5a4dDDGsystemdev$'); //encripta la contraseña

                        $datos = array("nombre" =>$_POST["nuevoNombre"], 
                                        "usuario" => $_POST["nuevoUsuario"],
                                        "password" => $passwordEncriptada,
                                        "perfil" => $_POST["nuevoPerfil"],
                                        "foto" => $ruta); //datos del form en array

                        $respuesta = ModeloUsuarios::mdlIngresarUsuario($tabla, $datos); // Respuesta del modelo al guradar en BD

                        if($respuesta == "ok"){
                            echo '<script>
                            Swal.fire({
                                title: "Registrado",
                                text: "El usuario se creo correctamente",
                                icon: "success",
                                showConfirmButton: true,
                                confirmButtonText: "Cerrar",
                                closeOnConfirm: false
                            }).then((result) => {
                                if (result.value) {
                                    window.location = "usuarios"                                
                                }
                            });                        
                        </script>';

                        }

                    }else{

                        echo '<script>
                                Swal.fire({
                                    title: "Advertencia",
                                    text: "El usuario no puede ir vacio o llevar caracteres especiales",
                                    icon: "warning",
                                    showConfirmButton: true,
                                    confirmButtonText: "Cerrar",
                                    closeOnConfirm: false
                                }).then((result) => {
                                    if (result.value) {
                                        window.location = "usuarios"                                    
                                    }
                                });                        
                            </script>';
                    }

            }

        }

     /* ===============================
            Mostar Usuario
        ================================== */

        static public function ctrMostrarUsuarios($item, $valor){

            $tabla = "usuarios"; // tabla de la BD a consultar
            $respuesta = ModeloUsuarios::MdlMostrarUsuarios($tabla, $item, $valor);
            return $respuesta;
        }

    /* ===============================
            Editar Usuario
        ================================== */

        static public function ctrEditarUsuario(){

            // ejecutar solo si viene informacion
            if(isset($_POST["editarUsuario"])){

                //traer datos a modificar, el nombre de usuario no sera modificable
                if(preg_match('/^[a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ ]+$/',$_POST["editarNombre"])){

                    //validación imagen
                    $ruta = $_POST["fotoActual"];

                    if(isset($_FILES["editarFoto"]["tmp_name"]) && !empty($_FILES["editarFoto"]["tmp_name"])){

                        //Redimensionar imagen
                        list($ancho, $alto) = getimagesize($_FILES["editarFoto"]["tmp_name"]);
                        $nuevoAncho = 500;
                        $nuevoAlto = 500;

                        //Crear directorio de imagen
                        $directorio = "vistas/img/usuarios/".$_POST["editarUsuario"];

                        // validar si existe imagen en BD
                        if(!empty($_POST["fotoActual"])){
                            unlink($_POST["fotoActual"]); //borrar ruta de imagen

                        }else{
                            mkdir($directorio, 0755); //crea el directorio con permisos de lectura y escritura
                        }                        

                        //aplicar metodos imagen jpeg
                        if($_FILES["editarFoto"]["type"] == "image/jpeg"){
                            //guardar imagen
                            $aleatorio = mt_rand(100,999);
                            $ruta = "vistas/img/usuarios/".$_POST["editarUsuario"]."/".$aleatorio.".jpg";

                            $origen = imagecreatefromjpeg($_FILES["editarFoto"]["tmp_name"]);
                            $destino = imagecreatetruecolor($nuevoAncho, $nuevoAlto);

                            imagecopyresized($destino, $origen, 0,0,0,0, $nuevoAncho, $nuevoAlto, $ancho, $alto);
                            imagejpeg($destino, $ruta);
                        }

                        //aplicar metodos imagen png
                        if($_FILES["editarFoto"]["type"] == "image/png"){
                            //guardar imagen
                            $aleatorio = mt_rand(100,999);
                            $ruta = "vistas/img/usuarios/".$_POST["editarUsuario"]."/".$aleatorio.".png";

                            $origen = imagecreatefrompng($_FILES["editarFoto"]["tmp_name"]);
                            $destino = imagecreatetruecolor($nuevoAncho, $nuevoAlto);

                            imagecopyresized($destino, $origen, 0,0,0,0, $nuevoAncho, $nuevoAlto, $ancho, $alto);
                            imagepng($destino, $ruta);
                        }

                    }

                    $tabla = "usuarios";

                    //si se cambia contraseña
                    if($_POST["editarPassword"] != ""){

                        if(preg_match('/^[a-zA-Z0-9]+$/',$_POST["editarPassword"])){

                            $passwordEncriptada = crypt($_POST["editarPassword"], '$2a$07$asxx54ahjppf45sd87a5a4dDDGsystemdev$'); //encripta la contraseña
                            
                        }else{

                            echo '<script>
                                Swal.fire({
                                    title: "Advertencia",
                                    text: "La contraseña no puede ir vacia o llevar caracteres especiales",
                                    icon: "warning",
                                    showConfirmButton: true,
                                    confirmButtonText: "Cerrar",
                                    closeOnConfirm: false
                                }).then((result) => {
                                    if (result.value) {
                                        window.location = "usuarios"                                    
                                    }
                                });                        
                            </script>';
                        }                       

                    }else{
                        $passwordEncriptada = $_POST["passwordActual"];
                    }

                    $datos = array("nombre" =>$_POST["editarNombre"], 
                                    "usuario" => $_POST["editarUsuario"],
                                    "password" => $passwordEncriptada,
                                    "perfil" => $_POST["editarPerfil"],
                                    "foto" => $ruta); //datos del form en array

                    $respuesta = ModeloUsuarios::mdlEditarUsuario($tabla, $datos);

                    if($respuesta = "ok"){

                        echo '<script>
                            Swal.fire({
                                title: "Registrado",
                                text: "Se edito usuario correctamente",
                                icon: "success",
                                showConfirmButton: true,
                                confirmButtonText: "Cerrar",
                                closeOnConfirm: false
                            }).then((result) => {
                                if (result.value) {
                                    window.location = "usuarios"                                
                                }
                            });                        
                        </script>';

                    }

                }else{

                    echo '<script>
                                Swal.fire({
                                    title: "Advertencia",
                                    text: "El nombre no puede ir vacio o llevar caracteres especiales",
                                    icon: "warning",
                                    showConfirmButton: true,
                                    confirmButtonText: "Cerrar",
                                    closeOnConfirm: false
                                }).then((result) => {
                                    if (result.value) {
                                        window.location = "usuarios"                                    
                                    }
                                });                        
                            </script>';

                }
            }
        }

    /* ===============================
            Eliminar Usuario
        ================================== */

        static public function ctrBorrarUsuario(){

            //validar si existe variable GET
            if(isset($_GET["idUsuario"])){

                // Eliminar en BD
                $tabla = "usuarios"; // tabla de la BD
                $datos = $_GET["idUsuario"];

                //eliminar foto
                if($_GET["fotoUsuario"] != ""){
                    unlink($_GET["fotoUsuario"]);
                    //remover directorio
                    rmdir('vistas/img/usuarios/'.$_GET["usuario"]);
                }

                $respuesta = ModeloUsuarios::mdlBorrarUsuario($tabla, $datos);

                if($respuesta == "ok"){

                    echo '<script>
                            Swal.fire({
                                title: "Usuario eliminado",
                                icon: "success",
                                showConfirmButton: true,
                                confirmButtonText: "Cerrar",
                                closeOnConfirm: false
                            }).then((result) => {
                                if (result.value) {
                                    window.location = "usuarios"                                
                                }
                            });                        
                        </script>';
                }

            }
        }


} /// fin clase principal 