# Nantiva pos
Desarrollo de sitio pos para nantiva - helados artesanales.

## Ubicacion de repositorio
- [ ] [Proyecto alojado en](https://gitlab.com/lloaizaa/pos_nantiva)

- [ ] [Agregar archivos usando linea de comandos](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) o inserte un repositorio Git existente con el siguiente comando:

```
cd existing_repo
git remote add origin https://gitlab.com/lloaizaa/pos_nantiva.git
git branch -M main
git push -uf origin main
```
***

## Nombre
NANTIVA gestor de ventas POS.

## Descripción
Sistema POS (Point of Sale) desarrollado en lenguaje de programación PHP en su versión 8.1.6, e integrando varios recursos que te permiten proporcionar características útiles para cualquier empresa que desee crear un punto de venta.

Se empleara el patrón Modelo-Vista-Controlador.

## Framework y librerias
Se toma como base para el front end la plantilla AdminLTE en su versión 2.4.3, el cual depende de dos marcos principales. El paquete descargable contiene ambas bibliotecas, por lo que no tiene que descargarlas manualmente.

estas son: 
    * Bootstrap 4.6
    * jQuery 3.5.1+
    * Popper.js 1.16.1+
    * entre otros plugins 

## Instalación
El sitema puede ser ejetudado de forma local o remota desde un hosting web, para ello necesitamos descargar los archivos del sistema y copiarlos en la siguiente ubicación:

 * servidor local - se copia los archivos el la carpeta htdocs de nuestro localhost

 * hosting web - seguir la documentación correspondiente del proveedor del servicio.

El sitema utiliza una base de datos mysql la cual es necesaria para el funcionamiento correcto y almacenamiento de cambios en el inventario, para crearla siga los siguientes pasos:

 * servidor local 
    - paso 1: ingresar a la url localhost/phpmyadmin.
    - paso 2: Haz clic en el botón de "Crear una nueva base de datos MySQL".
    - paso 3: Ponle como nombre nantiva_BD a tu nueva base de datos, selecciona cotejamiento 
            utf8-spanish-ci y clica  en "Confirmar".
    - paso 4: haz clic sobre el nombre de tu base de datos y"nantiva_BD" luego haz clic sobre el 
            botón de ‘Importar’.
    -paso 5: Con el botón de ‘Seleccionar Archivo’ busca en tu ordenador el fichero nantiva_BD.sql 
            y selecciónalo. Luego, con ‘Continuar’, inicia la importación.

    Si todo ha ido bien, aparecerá un mensaje de color verde. Y a la izquierda, aparecerán todas las tablas de su Base de Datos.

* hoting web
    -  seguir la documentación correspondiente del proveedor del servicio.    

## Modulos 
    La gestión del inventario.

    Gestión de ventas con diversos medios de pago.

    Impresión de facturas en PDF.

    Reportes de ventas en EXCEL.

    Informe de mejores clientes.

    Informe de los productos más vendidos.

    Informe de los mejores vendedores.

    Control del Stock de productos.

    Reporte diario.

    Reporte mensual.

    Reporte por rangos de fechas.

    Adaptado para usar en cualquier pantalla y dispositivo.

## Autores
- luis fernando loaiza acevedo

## Licencia
Para uso unico de Nantiva, se prohibe duplicación del proyecto sin autorizacion.
