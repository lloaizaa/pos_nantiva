<!-- ===============================
        Navbar 
    ================================== -->
  <header class="main-header">

    <!-- Logo -->
    <a href="inicio" class="logo">
      <!-- logo sidebar mini 50x50 -->
      <span class="logo-mini">
      <img src="vistas/img/plantilla/logo_50.png" class="img-responsive" style="padding: 10px">
      </span>

      <!-- logo sidebar Normal -->
      <span class="logo-lg">
      <img src="vistas/img/plantilla/logo_50.png" style="padding: 10px; width: 50px" >        
      <b>Nantiva</b> pos</span>
    </a>

    <!-- Navbar -->
    <nav class="navbar navbar-static-top">

      <!-- Boton Sidebar -->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">

          <!-- Fecha y hora -->
          <li class="messages-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-calendar"></i><span id="fecha" class="fecha" style="padding: 3px"></span>
            </a>
          </li>
          <li class="messages-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-clock-o"></i><span id="hora" class="hora" style="padding: 3px"></span>
            </a>
          </li>
          
          <!-- Cuenta Usuario -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">

              <?php //validar foto en BD
                if($_SESSION["foto"] != ""){
                  echo '<img src="'.$_SESSION["foto"].'" class="user-image" alt="Imangen de Perfil ">';
                }else{
                  echo '<img src="vistas/img/usuarios/default/anonimo.jpg" class="user-image" alt="Imangen de Perfil ">';
                }
              ?>
              
              <span class="hidden-xs"> <?php echo $_SESSION["nombre"]; //traer nombre de usuario ?></span>
              <i class="fa fa-sign-out" aria-hidden="true"></i>
            </a>

            <ul class="dropdown-menu">
              <!-- Imagen de usuario -->
              <li class="user-header">                
                <?php //validar foto en BD
                if($_SESSION["foto"] != ""){
                  echo '<img src="'.$_SESSION["foto"].'" class="img-circle" alt="Imangen de Perfil ">';
                }else{
                  echo '<img src="vistas/img/usuarios/default/anonimo.jpg" class="img-circle" alt="Imangen de Perfil ">';
                }
              ?>

                <p>
                  <?php echo $_SESSION["nombre"]. " - "  .$_SESSION["perfil"]; //traer nombre de usuario ?>
                  <small>registrado desde:  <?php echo $_SESSION["fecha"]; //traer nombre de usuario ?></small>
                </p>
              </li>
              
              <!-- Cuenta Usuario Footer-->
              <li class="user-footer">
                <div class="pull-right">
                  <a href="salir" class="btn btn-default btn-flat">Salir</a>
                </div>
              </li>
            </ul>
          </li>

        </ul>
      </div>
    </nav>
  </header>