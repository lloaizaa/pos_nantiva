<!-- ===============================
        Contenido 
    ================================== -->

<div class="content-wrapper">
  <!-- Encabezado de la pagina -->
  <section class="content-header">
    <h1>
      Administrar Usuarios
    </h1>
    <ol class="breadcrumb">
      <li><a href="inicio"><i class="fa fa-dashboard"></i>Inicio</a></li>
      <li class="active">Administrar usuarios</li>
    </ol>
  </section>

  <!-- Contenido Principal -->
  <section class="content">

    <!-- Default box -->
    <div class="box">
      <div class="box-header with-border">
        <button class="btn btn-primary" data-toggle="modal" data-target="#modalAgregarUsuario">Agregar Usuario</button>
      </div>

      <div class="box-body">
        <!-- tabla -->
        <table id="" class="table table-bordered table-striped dt-responsive tablas">
          <!-- cabecera de tabla -->
          <thead>
            <tr>
              <th style="width:10px;">#</th>
              <th>Nombre</th>
              <th>Usuario</th>
              <th>foto</th>
              <th>Prefil</th>
              <th>Estado</th>
              <th>Ultimo Ingreso</th>
              <th>Acciones</th>
            </tr>
          </thead>

          <!-- cuerpo de tabla -->
          <tbody>
            <tr>
              <td>1</td>
              <td>Usuario Administrador</td>
              <td>admin</td>
              <td> <img src="vistas/img/usuarios/default/anonimo.jpg" class="img-tumbnail" width="40px"> </td>
              <td>Administrador</td>
              <td> <button class="btn btn-success btn-xs">Activo</button></td>
              <td>X</td>
              <td>
                <div class="btn-group">
                  <button class="btn btn-warning btn-xs"> <i class="fa fa-pencil"></i> </button>
                  <button class="btn btn-danger btn-xs"> <i class="fa fa-trash"></i> </button>
                </div>
              </td>
            </tr>

            <tr>
              <td>2</td>
              <td>Usuario Administrador</td>
              <td>admin</td>
              <td> <img src="vistas/img/usuarios/default/anonimo.jpg" class="img-tumbnail" width="40px"> </td>
              <td>Administrador</td>
              <td> <button class="btn btn-success btn-xs">Activo</button></td>
              <td>X</td>
              <td>
                <div class="btn-group">
                  <button class="btn btn-warning btn-xs"> <i class="fa fa-pencil"></i> </button>
                  <button class="btn btn-danger btn-xs"> <i class="fa fa-trash"></i> </button>
                </div>
              </td>
            </tr>

            <tr>
              <td>3</td>
              <td>Usuario Administrador</td>
              <td>admin</td>
              <td> <img src="vistas/img/usuarios/default/anonimo.jpg" class="img-tumbnail" width="40px"> </td>
              <td>Administrador</td>
              <td> <button class="btn btn-light btn-xs">Inactivo</button></td>
              <td>X</td>
              <td>
                <div class="btn-group">
                  <button class="btn btn-warning btn-xs"> <i class="fa fa-pencil"></i> </button>
                  <button class="btn btn-danger btn-xs"> <i class="fa fa-trash"></i> </button>
                </div>
              </td>
            </tr>
          </tbody>

        </table>

      </div>
      <!-- /.tabla -->

    </div>
    <!-- /.box -->

  </section>
  <!-- /.fin contentido principal -->
</div>
<!-- /.content-wrapper -->


<!-- ===============================
        Modal Agregar Usuario 
    ================================== -->

<div class="modal fade" id="modalAgregarUsuario" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">

      <form role="form" method="post" enctype="multipart/form-data">
        <!-- encabezado de modal -->
        <div class="modal-header" style="background:#3c8dbc; color:white">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Nuevo Usuario</h4>
        </div>

        <!-- cuerpo de modal -->
        <div class="modal-body">
          <div class="box-body">
            <!-- formulario agregar usuario -->
            <div class="form-group">
              <!-- Campo Nombre -->
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                <input type="text" class="form-control" name="nuevoNombre" placeholder="Ingresar nombre" required>
              </div>
            </div>

            <div class="form-group">
              <!-- Campo Usuario -->
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-key"></i></span>
                <input type="text" class="form-control" name="nuevoUsuario" placeholder="Ingresar usuario" required>
              </div>
            </div>

            <div class="form-group">
              <!-- Campo Contraseña -->
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                <input type="password" class="form-control" name="nuevoPassword" placeholder="Ingresar contraseña" required>
              </div>
            </div>

            <div class="form-group">
              <!-- Campo Perfil -->
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-users"></i></span>
                <select class="form-control" name="nuevoPerfil">
                  <option value="">Seleccione un perfil</option>
                  <option value="Administrador">Administrador</option>
                  <option value="Vendedor">Vendedor</option>
                  <option value="Especial">control de inventario</option>
                </select>
              </div>
            </div>

            <div class="form-group">
              <!-- Campo Foto -->
              <div class="panel ">Subir foto</div>
              <input type="file" id="nuevaFoto" name="nuevaFoto">
              <p class="help-block">La imagen no debe superar 2MB</p>
              <img src="vistas/img/usuarios/default/anonimo.jpg" class="img-thumbnail" width="100px" alt="">
            </div>
            <!-- /.formulario agregar usuario -->
          </div>
        </div>

        <!-- pie de modal -->
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancelar</button>
          <button type="submit" class="btn btn-primary">Agregar usuario</button>
        </div>

      </form>

    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->