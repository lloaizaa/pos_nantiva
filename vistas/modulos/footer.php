<!-- ===============================
        Sidebar 
    ================================== -->
    
    <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.0
    </div>
    <strong>Copyright &copy; 2022 <a href="http://luisloaiza.ml" target="_blank">Luis Loaiza</a>.<strong>
  </footer>