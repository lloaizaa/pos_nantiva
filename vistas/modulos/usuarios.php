<!-- ===============================
        Contenido 
    ================================== -->

<div class="content-wrapper">
  <!-- Encabezado de la pagina -->
  <section class="content-header">
    <h1>
      Administrar Usuarios
    </h1>
    <ol class="breadcrumb">
      <li><a href="inicio"><i class="fa fa-dashboard"></i>Inicio</a></li>
      <li class="active">Administrar usuarios</li>
    </ol>
  </section>

  <!-- Contenido Principal -->
  <section class="content">

    <!-- Default box -->
    <div class="box">
      <div class="box-header with-border">
        <button class="btn btn-primary" data-toggle="modal" data-target="#modalAgregarUsuario">Agregar Usuario</button>
      </div>

      <div class="box-body">
        <!-- tabla -->
        <table id="" class="table table-bordered table-striped dt-responsive tablas">
          <!-- cabecera de tabla -->
          <thead>
            <tr>
              <th style="width:10px;">#</th>
              <th>Nombre</th>
              <th>Usuario</th>
              <th>foto</th>
              <th>Prefil</th>
              <th>Estado</th>
              <th>Ultimo Ingreso</th>
              <th>Acciones</th>
            </tr>
          </thead>

          <!-- cuerpo de tabla -->
          <tbody>

            <?php
              // tarer datos de BD a la tabla
              $item = null;
              $valor = null;
              $usuarios = ControladorUsuarios::ctrMostrarUsuarios($item, $valor);

              //recorrer array de tabla usuaios para traer datos
              foreach($usuarios as $key => $value){

                echo '<tr>
                <td>'.$value["id"].'</td>
                <td>'.$value["nombre"].'</td>
                <td>'.$value["usuario"].'</td>';

                if($value["foto"] != ""){ //validacion de foto
                  echo '<td> <img src="'.$value["foto"].'" class="img-tumbnail" width="40px"> </td>';
                }else{
                  echo '<td> <img src="vistas/img/usuarios/default/anonimo.jpg" class="img-tumbnail" width="40px"> </td>';
                }                

                echo '<td>'.$value["perfil"].'</td>';

                if($value["estado"] != 0){ //validar estado
                  echo '<td><button class="btn btn-success btn-xs btnActivar" idUsuario="'.$value["id"].'" estadoUsuario="0">Activo</button></td>';
                }else{
                  echo '<td><button class="btn btn-default btn-xs btnActivar" idUsuario="'.$value["id"].'" estadoUsuario="1">Inactivo</button></td>';
                }                

                echo '<td>'.$value["ultimo_login"].'</td>
                <td>
                  <div class="btn-group">
                    <button class="btn btn-warning btnEditarUsuario" idUsuario="'.$value["id"].'" data-toggle="modal" data-target="#modalEditarUsuario"> <i class="fa fa-pencil"></i> </button>
                    <button class="btn btn-danger btnEliminarUsuario" idUsuario="'.$value["id"].'" fotoUsuario="'.$value["foto"].'" Usuario="'.$value["usuario"].'"> <i class="fa fa-trash"></i> </button>
                  </div>
                </td>
              </tr>';
              }
            ?>

          </tbody>

        </table>

      </div>
      <!-- /.tabla -->

    </div>
    <!-- /.box -->

  </section>
  <!-- /.fin contentido principal -->
</div>
<!-- /.content-wrapper -->


<!-- ===============================
        Modal Agregar Usuario 
    ================================== -->

<div class="modal fade" id="modalAgregarUsuario" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">

      <form role="form" method="post" enctype="multipart/form-data">
        <!-- encabezado de modal -->
        <div class="modal-header" style="background:#3c8dbc; color:white">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Nuevo Usuario</h4>
        </div>

        <!-- cuerpo de modal -->
        <div class="modal-body">
          <div class="box-body">
            <!-- formulario agregar usuario -->
            <div class="form-group">
              <!-- Campo Nombre -->
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                <input type="text" class="form-control" name="nuevoNombre" placeholder="Ingresar nombre" required>
              </div>
            </div>

            <div class="form-group">
              <!-- Campo Usuario -->
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-key"></i></span>
                <input type="text" class="form-control" name="nuevoUsuario" id="nuevoUsuario" placeholder="Ingresar usuario" required>
              </div>
            </div>

            <div class="form-group">
              <!-- Campo Contraseña -->
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                <input type="password" class="form-control" name="nuevoPassword" placeholder="Ingresar contraseña" required>
              </div>
            </div>

            <div class="form-group">
              <!-- Campo Perfil -->
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-users"></i></span>
                <select class="form-control" name="nuevoPerfil">
                  <option value="">Seleccione un perfil</option>
                  <option value="Administrador">Administrador</option>
                  <option value="Vendedor">Vendedor</option>
                  <option value="Especial">control de inventario</option>
                </select>
              </div>
            </div>

            <div class="form-group">
              <!-- Campo Foto -->
              <div class="panel ">Subir foto</div>
              <input type="file" class="nuevaFoto" name="nuevaFoto">
              <p class="help-block">La imagen no debe superar 2MB</p>
              <img src="vistas/img/usuarios/default/anonimo.jpg" class="img-thumbnail previsualizar" width="100px" alt="">
            </div>
            <!-- /.formulario agregar usuario -->
          </div>
        </div>

        <!-- pie de modal -->
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancelar</button>
          <button type="submit" class="btn btn-primary">Agregar usuario</button>
        </div>

        <?php
          $crearUsuario = new ControladorUsuarios();
          $crearUsuario -> ctrCrearUsuario();
        ?>

      </form>

    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal Agregar Usuario -->

<!-- ===============================
        Modal Editar Usuario 
    ================================== -->

    <div class="modal fade" id="modalEditarUsuario" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">

      <form role="form" method="post" enctype="multipart/form-data">
        <!-- encabezado de modal -->
        <div class="modal-header" style="background:#3c8dbc; color:white">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Editar Usuario</h4>
        </div>

        <!-- cuerpo de modal -->
        <div class="modal-body">
          <div class="box-body">
            <!-- formulario agregar usuario -->
            <div class="form-group">
              <!-- Campo Nombre -->
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                <input type="text" class="form-control" id="editarNombre" name="editarNombre" value="" required>
              </div>
            </div>

            <div class="form-group">
              <!-- Campo Usuario -->
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-key"></i></span>
                <input type="text" class="form-control" id="editarUsuario" name="editarUsuario" value="" required readonly>
              </div>
            </div>

            <div class="form-group">
              <!-- Campo Contraseña -->
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                <input type="password" class="form-control" name="editarPassword" placeholder="Cambiar contraseña">
                <input type="hidden" id="passwordActual" name="passwordActual">
              </div>
            </div>

            <div class="form-group">
              <!-- Campo Perfil -->
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-users"></i></span>
                <select class="form-control" name="editarPerfil">
                  <option value="" id="editarPerfil"></option>
                  <option value="Administrador">Administrador</option>
                  <option value="Vendedor">Vendedor</option>
                  <option value="Control de inventario">control de inventario</option>
                </select>
              </div>
            </div>

            <div class="form-group">
              <!-- Campo Foto -->
              <div class="panel ">Subir foto</div>
              <input type="file" class="nuevaFoto" name="editarFoto">
              <p class="help-block">La imagen no debe superar 2MB</p>
              <img src="vistas/img/usuarios/default/anonimo.jpg" class="img-thumbnail previsualizar" width="100px" alt="">
              <input type="hidden" name="fotoActual" id="fotoActual">
            </div>
            <!-- /.formulario agregar usuario -->
          </div>
        </div>

        <!-- pie de modal -->
        <div class="modal-footer">
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancelar</button>
          <button type="submit" class="btn btn-primary">Modificar usuario</button>
        </div>

        <?php
         $editarUsuario = new ControladorUsuarios();
         $editarUsuario -> ctrEditarUsuario();
        ?>

      </form>

    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal Editar Usuario  -->

<?php
//ejecutar metodo para eliminar usuario
  $borrarUsuario = new ControladorUsuarios();
  $borrarUsuario -> ctrBorrarUsuario();
?>