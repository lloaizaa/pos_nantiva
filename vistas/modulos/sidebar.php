<!-- ===============================
        Sidebar 
    ================================== -->

  <!-- Columna Izquierda - contenido -->
  <aside class="main-sidebar">

    <!-- sidebar -->
    <section class="sidebar">

      <!-- sidebar menu -->
      <ul class="sidebar-menu" data-widget="tree">

        <li class="header">Menu de navegación</li>

        <!-- inicio -->
        <li>
          <a href="inicio" class="active">
            <i class="fa fa-home"></i> <span>inicio</span>            
          </a>
        </li>

        <!-- ventas -->
        <li class="treeview">
          <a href="">
            <i class="fa fa-list-ul"></i> <span>Ventas</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-double-right pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="crear-venta"><i class="fa fa-circle-o"></i>Nueva venta</a></li>
            <li><a href="ventas"><i class="fa fa-circle-o"></i>Administrar Ventas</a></li>
            <li><a href="reportes"><i class="fa fa-circle-o"></i>Reporte de Ventas</a></li>
          </ul>
        </li>

        <!-- productos -->
        <li>
          <a href="productos">
            <i class="fa fa-product-hunt"></i> <span>Productos</span>            
          </a>
        </li>

        <!-- categorias -->
        <li>
          <a href="categorias">
            <i class="fa fa-th"></i> <span>Categorías</span>            
          </a>
        </li>

        <!-- Clientes -->
        <li>
          <a href="clientes">
            <i class="fa fa-users"></i> <span>Clientes</span>            
          </a>
        </li>

        <!-- usuarios -->
        <li>
          <a href="usuarios">
            <i class="fa fa-user-circle-o"></i> <span>Usuarios</span>            
          </a>
        </li>      
        
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
