<!-- ===============================
        Contenido 
    ================================== -->

    <div class="content-wrapper">
    <!-- Encabezado de la pagina -->
    <section class="content-header">
      <h1>
        Pagína No encontrada
      </h1>
      <ol class="breadcrumb">
        <li><a href="inicio"><i class="fa fa-dashboard"></i>Inicio</a></li>
        <li class="active">error 404</li>
      </ol>
    </section>

    <!-- Contenido Principal -->
    <section class="content">

      <!-- 404 -->

      <div class="error-page">
        <h2 class="headline text-primary">404</h2>
      </div>

      <div class="error-content">
        <h3>
            <i class="fa fa-warning text-primary"></i>
            !Ooops¡ Página no encontrada.
        </h3>

        <p>Ingresa al menú laterar y allí podrás encontrar las paginas disponibles
            También puedes regresar haciendo <a href="inicio" >click aquí.</a>
        </p>
      </div>

      <!-- /.404 -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->