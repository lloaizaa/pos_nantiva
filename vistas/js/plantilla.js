/*=============================================
    sidebarmenu
=============================================*/
  $(document).ready(function () {
    $('.sidebar-menu').tree()
  })

  /*=============================================
    fecha y hora
=============================================*/

var inicializarHora = function(){

    var fechaActual = new Date();
    var tiempoHoras = fechaActual.getHours();
    var tiempoMinutos = fechaActual.getMinutes();
    var tiempoSegundos = fechaActual.getSeconds();

    var mesActual = fechaActual.getMonth();
    var diaActual = fechaActual.getDay();
    var diaDelMes = fechaActual.getDate();
    var aActual = fechaActual.getFullYear();
    var amOpm;

    var meses =["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio",
                "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];
    var esteMes = meses[mesActual];

    var diasDeLaSemana = ["Domingo", "Lunes","Martes","Miercoles","Jueves","Viernes","Sabado"];
    var diaDeHoy = diasDeLaSemana[diaActual];

    amOpm = (tiempoHoras > 12) ? "pm" : "am";
    tiempoHoras = (tiempoHoras > 12) ? tiempoHoras - 12 : tiempoHoras;
    tiempoHoras = (tiempoHoras < 10) ? "0" + tiempoHoras : tiempoHoras;
    tiempoMinutos = (tiempoMinutos < 10) ? "0" + tiempoMinutos : tiempoMinutos;
    tiempoSegundos = (tiempoSegundos < 10) ? "0" + tiempoSegundos : tiempoSegundos;

    document.getElementById("fecha").innerHTML = diaDeHoy + " " + diaDelMes + " de " + esteMes + " del " + aActual;

    document.getElementById("hora").innerHTML = tiempoHoras + ":" + tiempoMinutos + ":" + tiempoSegundos + " " + amOpm;
  
}
inicializarHora();
setInterval(inicializarHora, 1000);

  /*=============================================
    Data Table
=============================================*/

$(".tablas").DataTable({

  "language": {
      "sProcessing": "Procesando...",
      "sLengthMenu": "Mostrar _MENU_ registros",
      "sZeroRecords": "No se encontraron resultados",
      "sEmptyTable": "Ningún dato disponible en esta tabla",
      "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_",
      "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
      "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
      "sSearch": "Buscar:",
      "sInfoThousands": ",",
      "sLoadingRecords": "Cargando...",
      "oPaginate": {
          "sFirst": "Primero",
          "sLast": "Último",
          "sNext": "Siguiente",
          "sPrevious": "Anterior"
      },
      "aria": {
          "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
          "sSortDescending": ": Activar para ordenar la columna de manera descendente"
      }

  }

});

  /*=============================================
    
=============================================*/