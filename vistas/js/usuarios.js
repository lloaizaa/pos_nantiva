  /*=============================================
    Subir foto de usuario
=============================================*/

$(".nuevaFoto").change(function(){

    var imagen = this.files[0];

    // ==== Validar formato de imagen ====
    if(imagen["type"] != "image/jpeg" && imagen["type"] != "image/png"){
        $(".nuevaFoto").val("");
            Swal.fire({
                icon: "error",
                title: "Error al subir imagen",
                text: "La imagen debe estar en formato JPG o PNG",
                confirmButtonText: "¡Cerrar!"
            });

        // ==== Validar tamaño de imagen ====
    }else if(imagen["size"] > 2000000){
        $(".nuevaFoto").val("");
            Swal.fire({
                icon: "warning",
                title: "Error al subir imagen",
                text: "La imagen no debe ser superior a 2MB",
                confirmButtonText: "¡Cerrar!"
            });

         // ==== Amacenar imagen en variable ====
    }else {
        var datosImagen = new FileReader;
        datosImagen.readAsDataURL(imagen);

        $(datosImagen).on("load", function(event){
            var rutaImagen = event.target.result;

            // ==== mostrar vista previa de imagen ====
            $(".previsualizar").attr("src", rutaImagen);
        })
    }

})

  /*=============================================
    Editar usuario
=============================================*/

$(document).on("click", ".btnEditarUsuario", function(){

    // ==== Capturar información en variable ====
    var idUsuario = $(this).attr("idUsuario");

    // ==== traer datos del id capturado ====    
    var datos = new FormData();
    datos.append("idUsuario", idUsuario);

    // ==== ejecutar acción ====
    $.ajax({
        url: "ajax/usuarios.ajax.php",
        method: "POST",
        data: datos,
        cache: false,
        contentType: false,
        processData: false,
        dataType: "json",
        success: function(respuesta){
            
            $("#editarNombre").val(respuesta["nombre"]);
            $("#editarUsuario").val(respuesta["usuario"]);
            $("#editarPerfil").html(respuesta["perfil"]);
            $("#editarPerfil").val(respuesta["perfil"]);
            $("#fotoActual").val(respuesta["foto"]);

            $("#passwordActual").val(respuesta["password"]);

            // ==== traer foto ====
            if(respuesta["foto"] != ""){
                $(".previsualizar").attr("src", respuesta["foto"]);
            }
        }
    });
})

  /*=============================================
    Activar usuario
=============================================*/

$(document).on("click", ".btnActivar", function(){

    //almacenar id de usuario
    var idUsuario = $(this).attr("idUsuario");
    var estadoUsuario = $(this).attr("estadoUsuario");

    //solicitar actualización de estado en BD
    var datos = new FormData();
    datos.append("activarId", idUsuario);
    datos.append("activarUsuario", estadoUsuario);

    // ==== ejecutar acción ====
    $.ajax({
        url: "ajax/usuarios.ajax.php",
        method: "POST",
        data: datos,
        cache: false,
        contentType: false,
        processData: false,
        success: function(respuesta){

            // si es pantalla pequeña
            if(window.matchMedia("(max-width:767px)").matches){
                Swal.fire({
                    icon: "success",
                    title: "El usuario cambio de estado",
                    ConfirmButton: "Aceptar"
                  }).then(function(result){
                    if(result.value){
                        window.location = "usuarios"
                    }
                  });      
            }
        }
    })

    //cambiar apariencia de boton segun estado
    if(estadoUsuario == 0){
        $(this).removeClass('btn-success');
        $(this).addClass('btn-default');
        $(this).html('Inactivo');
        $(this).attr('estadoUsuario', 1);

    }else{
        $(this).addClass('btn-success');
        $(this).removeClass('btn-default');
        $(this).html('Activo');
        $(this).attr('estadoUsuario', 0);

    }

})

  /*=============================================
    Validar usuario registrado
=============================================*/

$("#nuevoUsuario").change(function(){

    // ==== remover mensaje de alerta ====
    $(".callout").remove();
        // ==== temporizar alerta ====
        setTimeout(function() {
        $(".callout").fadeOut(1500);
        },3000);

    var usuario = $(this).val();
    var datos = new FormData();
    datos.append("validarUsuario", usuario);

    // ==== ejecutar acción ====
    $.ajax({
        url: "ajax/usuarios.ajax.php",
        method: "POST",
        data: datos,
        cache: false,
        contentType: false,
        processData: false,
        dataType: "json",
        success: function(respuesta){
            
            if(respuesta){
                // ==== si viene respuesta informar al usuario ====
                $("#nuevoUsuario").parent().after('<div class="callout callout-danger">Este usuario ya existe, por favor seleccione otro</div>');
                $("#nuevoUsuario").val("");
            }
        }
    })    
})

  /*=============================================
    Validar usuario registrado
=============================================*/

$(document).on("click", ".btnEliminarUsuario", function(){

    var idUsuario = $(this).attr("idUsuario");
    var fotoUsuario = $(this).attr("fotoUsuario");
    var usuario = $(this).attr("Usuario");

    // ==== alertar al usuario ====
    Swal.fire({
        title: '¿Desea eliminar este Usuario?',
        text: "!Recuerde que de eliminar el usuario, este no podra ingresar al sistema y perdera toda su información¡",
        icon: 'info',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Cancelar',
        confirmButtonText: 'Si, elimiar usuario'        
      }).then((result) => {
        /* si se confirma*/
        if (result.value) {

            // ==== Capturar datos de usuario a eliminar ====
            window.location = "index.php?ruta=usuarios&idUsuario="+idUsuario+"&usuario="+usuario+"&fotoUsuario="+fotoUsuario;

          Swal.fire('Saved!', '', 'success')
        } else if (result.isDenied) {
          Swal.fire('usuario no eliminado')
        }
      })


})