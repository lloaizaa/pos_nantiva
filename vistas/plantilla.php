<?php
// metodo para variable  de sesión
session_start();
?>

<!DOCTYPE html>
<html>

<head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">

      <title>Nantiva | pos</title>

      <!-- Tell the browser to be responsive to screen width -->
      <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

      <link rel="icon" href="vistas/img/plantilla/favicon.png">

      <!-- ===============================
        Plugins de CSS 
    ================================== -->

      <!-- Bootstrap 3.3.7 -->
      <link rel="stylesheet" href="vistas/bower_components/bootstrap/dist/css/bootstrap.min.css">

      <!-- Font Awesome -->
      <link rel="stylesheet" href="vistas/bower_components/font-awesome/css/font-awesome.min.css">

      <!-- Ionicons -->
      <link rel="stylesheet" href="vistas/bower_components/Ionicons/css/ionicons.min.css">

      <!-- Theme style -->
      <link rel="stylesheet" href="vistas/dist/css/AdminLTE.css">

      <!-- AdminLTE Skins. Choose a skin from the css/skins -->
      <link rel="stylesheet" href="vistas/dist/css/skins/_all-skins.min.css">

      <!-- Google Font -->
      <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

      <!-- DataTables -->
      <link rel="stylesheet" href="vistas/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
      <link rel="stylesheet" href="vistas/bower_components/datatables.net-bs/css/responsive.bootstrap.min.css">

      <!-- ===============================
        Plugins de Javascript 
    ================================== -->

      <!-- jQuery 3 -->
      <script src="vistas/bower_components/jquery/dist/jquery.min.js"></script>

      <!-- Bootstrap 3.3.7 -->
      <script src="vistas/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

      <!-- SlimScroll -->
      <script src="vistas/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>

      <!-- FastClick -->
      <script src="vistas/bower_components/fastclick/lib/fastclick.js"></script>

      <!-- AdminLTE App -->
      <script src="vistas/dist/js/adminlte.min.js"></script>

      <!-- DataTables -->
      <script src="vistas/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
      <script src="vistas/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
      <script src="vistas/bower_components/datatables.net-bs/js/dataTables.responsive.min.js"></script>
      <script src="vistas/bower_components/datatables.net-bs/js/responsive.bootstrap.min.js"></script>

      <!-- SweetAlert 2 -->
      <script src="vistas/plugins/SweetAlert2/sweetAlert2.all.js"></script>

</head>

<!-- ===============================
        Cuerpo de documento 
    ================================== -->

<body class="hold-transition skin-blue sidebar-mini login-page">
      <!-- Site wrapper -->


      <?php
      // validar si se inicio sesion
      if (isset($_SESSION["iniciarSesion"]) && $_SESSION["iniciarSesion"] == "ok") {

            echo '<div class="wrapper">'; // inica contenedor wrapper

            /* ===============================
            navbar y Sidebar
      ================================== */

            include "modulos/navbar.php";

            include "modulos/sidebar.php";

            /* ===============================
            Contenido de paginas - content-wrapper
      ================================== */

            // validar url y llamar ruta amigable
            if (isset($_GET["ruta"])) {

                  if (
                        $_GET["ruta"] == "inicio" ||
                        $_GET["ruta"] == "usuarios" ||
                        $_GET["ruta"] == "categorias" ||
                        $_GET["ruta"] == "productos" ||
                        $_GET["ruta"] == "clientes" ||
                        $_GET["ruta"] == "ventas" ||
                        $_GET["ruta"] == "crear-venta" ||
                        $_GET["ruta"] == "reportes" ||
                        $_GET["ruta"] == "salir"
                  ) {

                        //redirigir vista segun url
                        include "modulos/" . $_GET["ruta"] . ".php";
                  } else {

                        //si se introduce una ruta inexistente redirigir a 404
                        include "modulos/404.php";
                  }
            } else {
                  //si se introduce una ruta inexistente redirigir a 404
                  include "modulos/inicio.php";
            }

            /* ===============================
            Footer 
        ================================== */

            include "modulos/footer.php";

            echo '</div>>'; // finaliza contenedor wrapper

      } else {
            //si no ha iniciado sesion redirigir a login
            include "modulos/login.php";
      }

      ?>

      <!-- JS personalizado -->
      <script src="vistas\js\plantilla.js"></script>
      <script src="vistas\js\usuarios.js"></script>

</body>

</html>