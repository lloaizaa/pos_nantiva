<?php

// vincular controladores y modelos
require_once "../controladores/usuarios.controlador.php";
require_once "../modelos/usuarios.modelo.php";

// clase principal para usuarios
class AjaxUsuarios{

    /* ===============================
            Editar Usuarios
        ================================== */
        public $idUsuario; 

        public function ajaxEditarUsuario(){

            // solicitar al controlador

            $item = "id";
            $valor = $this->idUsuario;
            $respuesta = ControladorUsuarios::ctrMostrarUsuarios($item, $valor);

            echo json_encode($respuesta);

        }

    /* ===============================
            Activar Usuario
        ================================== */

        public $activarUsuario;
        public $activarId;

        public function ajaxActivarUsuario(){

            $tabla = "usuarios";
            $item1 = "estado";
            $valor1 = $this->activarUsuario;
            
            $item2 = "id";
            $valor2 = $this->activarId;

            // solicitar respuesta al modelo
            $respuesta = ModeloUsuarios::mdlActualizarUsuario($tabla, $item1, $valor1, $item2, $valor2);           

        }

    /* ===============================
            validar usuario registrado
        ================================== */

        public $validarUsuario;
        public function ajaxValidarUsuario(){

            // solicitar al controlador

            $item = "usuario";
            $valor = $this->validarUsuario;
            $respuesta = ControladorUsuarios::ctrMostrarUsuarios($item, $valor);

            echo json_encode($respuesta);

        }


}// fin clase principal 

// ejecutar metodo para editar usuarios
if(isset($_POST["idUsuario"])){
    $editar = new AjaxUsuarios();
    $editar -> idUsuario =$_POST["idUsuario"];
    $editar -> ajaxEditarUsuario();
}

// ejecutar metodo para activar usuarios
if(isset($_POST["activarUsuario"])){
    $activarUsuario = new AjaxUsuarios();
    $activarUsuario -> activarUsuario = $_POST["activarUsuario"];
    $activarUsuario -> activarId = $_POST["activarId"];
    $activarUsuario -> ajaxActivarUsuario();
}

// ejecutar metodo para validar usuario registrado
if(isset($_POST["validarUsuario"])){
    $valUsuario = new AjaxUsuarios();
    $valUsuario -> validarUsuario = $_POST["validarUsuario"];
    $valUsuario -> ajaxValidarUsuario();
}

